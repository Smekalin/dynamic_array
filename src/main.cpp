#include <iostream>

#include "DynamicArray.h"

int main()
{
    DynamicArray<double> array;

    int element = 88.0;

    array.addElement(element);

    int a = array[0];

    std::cout << a << std::endl;

    array.addElement(13);

    std::cout << array.getElement(1) << std::endl;

    array[2] = 10;

    std::cout << array[0] << std::endl;

    return 0;
}