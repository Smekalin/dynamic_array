#ifndef _DYNAMICARRAY_IMPL_H_
#define _DYNAMICARRAY_IMPL_H_

#include "DynamicArray.h"
#include <algorithm>
#include <assert.h>
#include "string.h"

#define INITIAL_SIZE 10

template <typename T>
T& DynamicArray<T>::operator[](unsigned int index)
{
    assert(index >= 0 && index <= realSize && buffer != 0);
    return buffer[index];
}

template <typename T>
T DynamicArray<T>::getElement (unsigned int index) const
{
    assert(index >= 0 && index <= realSize && buffer != 0);

    return buffer[index];
}

template <typename T>
void DynamicArray<T>::grow()
{
    unsigned int newBufferSize = std::max((static_cast<int>(bufferSize)*2), INITIAL_SIZE);

    T* newBuffer = new T[newBufferSize];

    memcpy(newBuffer, buffer, sizeof(unsigned int) * realSize);

    delete[] buffer;

    buffer = newBuffer;

    bufferSize = newBufferSize;
}

template <typename T>
void DynamicArray<T>::addElement(T element)
{
    if (realSize == bufferSize)
    {
        grow();
    }

    assert(realSize < bufferSize && buffer != 0);

    buffer[realSize++] = element;
}

#endif //_DYNAMICARRAY_IMPL_H_