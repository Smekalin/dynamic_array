#ifndef _DYNAMICARRAY_H_
#define _DYNAMICARRAY_H_

template <typename T>
class DynamicArray
{
public:
    DynamicArray():
    buffer(0),
    bufferSize(0),
    realSize(0)
    {}

    ~DynamicArray(){delete[] buffer;}

    T getElement(unsigned int index) const;
    void addElement(T element);

    T operator[](unsigned int index) const {return getElement(index);} // rvalue
    T& operator[](unsigned int index); // lvalue

private:
    T* buffer;
    unsigned int bufferSize;
    unsigned int realSize;

    void grow();
};

#include "DynamicArray_impl.h"

#endif //_DYNAMICARRAY_H_